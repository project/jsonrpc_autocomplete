[//]: # ( phpcs:ignoreFile)

**A simple module to expose Drupal's autocomplete handlers via JSONRPC. Auto create functionality provided by core has not been tested.**

# Why use this module?
The main benefit to this module's autocomplete is the use of Drupal's selection handlers allowing for a decoupled entity autocomplete experience that is nearly identical to Drupal's core entity reference field. The module differs slightly from core in the data it returns, core only returns preformatted data to render a select field whereas this module returns additional information to use as needed.

The other benefit to using selection handlers vs direct querying of JSON:API or JSON:API Search API is that a selection can easily be validated on the backend using the exact same selection logic that was used for the front end.

# Request
To run a search you can use a POST request
```json
{
  "jsonrpc": "2.0",
  "method": "autocomplete",
  "id": "autocomplete",
  "params": {
    "target_type": "node",
    "selection_handler": "default",
    "selection_settings": {
      "target_bundles": null,
      "sort": {
        "field": "_none",
        "direction": "ASC"
      }
    },
    "query": "search string goes here"
  }
}
```
or a GET request that uses the url encoded POST data
```
https://example.org/jsonrpc?query=%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22autocomplete%22%2C%22id%22%3A%22autocomplete%22%2C%22params%22%3A%7B%22target_type%22%3A%22node%22%2C%22selection_handler%22%3A%22default%22%2C%22selection_settings%22%3A%7B%22target_bundles%22%3Anull%2C%22sort%22%3A%7B%22field%22%3A%22_none%22%2C%22direction%22%3A%22ASC%22%7D%7D%2C%22query%22%3A%22h%22%7D%7D
```

## Selection settings structure
The selection_settings value supports all options defined for the chosen handler. The settings should be formatted as a JSON object structured identically to the PHP or YAML versions of the settings.

# Response
```json
{
  "jsonrpc": "2.0",
  "id": "autocomplete",
  "result": [
    {
      "id": "5",
      "uuid": "df078d9e-b34e-4719-a9fc-b8946b63acaf",
      "type": "node",
      "bundle": "page",
      "label": "Example"
    },
    {
      "id": "1",
      "uuid": "d93d7f5b-f36d-4a76-ae9d-24bc56e06d0b",
      "type": "node",
      "bundle": "page",
      "label": "Homepage"
    }
  ]
}
```
