<?php

namespace Drupal\jsonrpc_autocomplete\Plugin\jsonrpc\Method;

use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Run a search for entities.
 *
 * @JsonRpcMethod(
 *   id = "autocomplete",
 *   usage = @Translation("Search for entities."),
 *   access = {"administer site configuration"},
 *   params = {
 *     "target_type" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"},
 *       required=true
 *     ),
 *     "selection_handler" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"},
 *       required=true
 *     ),
 *     "selection_settings" = @JsonRpcParameterDefinition(
 *       schema={"type": "object"}
 *     ),
 *     "query" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"}
 *     ),
 *   }
 * )
 */
class Autocomplete extends JsonRpcMethodBase {

  /**
   * The entity matcher.
   *
   * @var \Drupal\Core\Entity\EntityAutocompleteMatcherInterface
   */
  protected EntityAutocompleteMatcherInterface $matcher;

  /**
   * Autocomplete constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityAutocompleteMatcherInterface $matcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->matcher = $matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('jsonrpc_autocomplete.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ParameterBag $params) {
    $target_type = $params->get('target_type');
    $selection_handler = $params->get('selection_handler');
    $selection_settings = $params->get('selection_settings');
    $query = $params->get('query');

    $matches = [];
    // Get the typed string from the URL, if it exists.
    if ($query) {
      $typed_string = mb_strtolower($query);

      /* This section duplicates what is done in core's
       * \Drupal\system\Controller\EntityAutocompleteController but with the
       * hash check removed. In
       * https://www.drupal.org/project/drupal/issues/2490420 the hash check
       * was introduced to
       *   a) correct a security issue when using unseralize with user input
       *   b) prevent a sql injection issue in the the match query
       *   c) prevent exposing data through an open endpoint
       * Removing the hash check here should be fine now because
       *   a) the unseralize has been replaced with a json_decode
       *   b) the match query was hardened in
       *      https://www.drupal.org/project/drupal/issues/2492967
       *   c) Drupal now has multiple api systems that expose all the data a
       *      user has access to. With the matcher running an entity access
       *      check this endpoint probably exposes less than other endpoints.
       */
      $matches = $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $typed_string);
    }

    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public static function outputSchema() {
    return [
      'type' => 'array',
      'patternProperties' => [
        'id' => ['type' => 'string'],
        'uuid' => ['type' => 'string'],
        'type' => ['type' => 'string'],
        'bundle' => ['type' => 'string'],
        'label' => ['type' => 'string'],
      ],
    ];
  }

}
